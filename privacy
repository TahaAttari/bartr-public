Privacy policy

This privacy policy (our "Privacy Policy") sets out the information gathering and dissemination practices of Fava. and/or its affiliates and subsidiaries (references to, "Fava", "we", "us", or "our" are references Fava.) in the use of our website (our "Website") and/or or mobile application (together the “Platform”) , including the trade listing service (collectively with the Platform, the "Services"). By using our Services, you are consenting to our Privacy Policy and the collection, use and disclosure of your personal information by Fava as outlined herein. If our Privacy Policy is not acceptable to you, please do not submit any of your personal information.

We may update the Privacy Policy from time to time and you are responsible for periodically reviewing the most current version of the Privacy Policy on our Website. Your continued use of our Services or submission of information will be deemed your conclusive acceptance of our updated Privacy Policy.

In our Privacy Policy, the phrase "Service Providers" includes, but is not limited to, any licensors, suppliers, information providers or other third parties that provide, from time to time, any data, information, content, application, tool or service for purposes of the Services.

Collecting, Using and Disclosing Information

Our Privacy Policy describes our policies regarding the collection, use and disclosure of the personal information that we (or our Service Providers on our behalf) collect about you through our Services such as your name, address, phone number, fax number, e-mail address or payment information as well as your Fava transaction history. We may collect this information when you subscribe to, or sign up for, certain services, tools or features that we provide, when you register for seminars or other programs that we offer, when you fill out forms made available through our Services, when you enter a promotion or contest, when you complete a survey, when you e-mail us with general inquiries or with your comments or suggestions, or otherwise in connection with your use of our Services. We will limit the personal information we collect to what we need for the purposes for which it was collected, and will use such personal information for such purposes. We may also use personal information we collect to provide you with information on products, services and events that we or third parties offer that we believe may be of interest to you. If we wish to use your personal information for any other purpose, we will obtain your consent before using the information.

WHAT TYPES OF INFORMATION DOES Fava GATHER ABOUT ITS USERS?

We receive, store and process information that you make available to us when accessing or using our Platform and Services. In particular:

When you register or update the details of your user account, or when you supply ID verification information, Fava collects your name, username, email address, and, if you opt-in to location services, your location. This personal information is used to administer your account, provide you the Services and contact you, including sending you notifications about the Services.
If you opt for second-factor verification then we will also collect your phone number to use for second-factor authentication.
When you access or use the Platform, such as to search for or post, make or accept trades / transactions, post comments or reviews, or communicate with other users, Fava will keep track of your trades, posts, comments and likes, ratings and reviews, and transaction history so that you have a record of your activity on the Services.
If you use the chat function of the Services, Fava keeps a record of your chat history.
If you link your account on a third party site (e.g. Facebook) to your Fava account Fava will obtain the Personal Information that you have provided to the third party site, to the extent allowed by your settings with the third party site and authorized by you; and
Fava also keeps track of your usage patterns, for example that pages that you visit, for the analytics purposes so that Fava can monitor and improve the Services.

Storing of Information and Restricting Access

We may store your personal information (in encrypted form where we believe it to be highly sensitive) in electronic databases or e-mail boxes hosted by us or our Service Providers, for periods of time and with safeguards that we believe are reasonable depending on the nature and sensitivity of the information. Access to the information is restricted in accordance with our security protocols.

Fava stores its Services data, including personal information, on servers located in the United States where it is subject to U.S. law and may be made available to U.S. government authorities under court order or other lawful access regimes. By using the Services, you consent to your personal information being stored and processed in the United States.

Certain features on our Website are password-protected to prevent unauthorized access. You are responsible for the confidentiality and use of such passwords.

Due to the nature of Internet communications and evolving technologies, we cannot provide assurance that the personal information we collect will remain free from loss, interception, misuse or alteration by third parties and we and our Service Providers shall have no liability for any loss, interception, misuse or alteration.

Automatic Collection of Information

In some cases, we may collect information about you that may or may not be personally-identifiable. Examples of this type of information include your Internet protocol (IP) address, the type of Internet browser you are using, the type of computer operating system you are using, and the advertisement or domain name of the website from which you linked to our Services. The collection, use, disclosure and storage of such information will be in accordance with our Privacy Policy, including for the purpose of optimizing user experience of our Services.

Mobile Data

When you use certain features of the Platform, in particular our mobile applications we may receive, store and process different types of information about your location, including general information (e.g., IP address, zip code) and more specific information (e.g., GPS-based functionality on mobile devices used to access the Platform or specific features of the platform). If you access the Platform through a mobile device and you do not want your device to provide us with location-tracking information, you can disable the GPS or other location-tracking functions on your device, provided your device allows you to do this. See your device manufacturer's instructions for further details.

Log Data

We may also receive, store and process Log Data, which is information that is automatically recorded by our servers whenever you access or use the Platform, regardless of whether you are registered with Fava or logged in to your Fava account, such as your IP Address, the date and time you access or use the Platform, the hardware and software you are using, referring and exit pages and URLs, the number of clicks, pages viewed and the order of those pages, and the amount of time spent on particular pages.

Cookies

Some pages on our Website use a technology called "cookies". A cookie is a token that a server gives to your browser when you access a website. Cookies are capable of storing many types of data. Cookies may be placed by us or a third party. Cookies help provide additional functionality to our Website or help provide and analyze our Website traffic and usage information. For instance, our server may set a cookie that keeps you from having to enter a password more than once during a visit to our Website. In all cases in which cookies are used, we will not collect personally-identifiable information except with your permission. With most Internet browsers, you can erase cookies from your computer hard drive, block all cookies, or receive a warning before a cookie is stored. Please refer to your browser instructions or help screen to learn more about these functions.

Third-party social plugins

Our Platform may use social plugins which are provided and operated by third-party companies, such as Facebook's Login / Share with Instagram.

As a result of this, you may send to the third-party company the information that you are viewing on a certain part of our Platform. If you are not logged into your account with the third-party company, then the third party may not know your identity. If you are logged into your account with the third-party company, then the third party may be able to link information about your visit to our Platform to your account with them. Similarly, your interactions with the social plugin may be recorded by the third party.

Please refer to the third party's privacy policy to find out more about its data practices, such as what data is collected about you and how the third party uses such data.

Meetups

The Platform may allow registered account holders to organize, search for or participate in offline events ("Meetups") in selected cities.

If you organize a Meetup or indicate that you will attend one, this information, together with some of your public information (such as your profile picture and public profile page) and any messages that you post about that Meetup, will be visible to users who browse the event. However, Fava will never disclose where you are staying to another meetup user.

Groups

The Platform may allow registered account holders to participate in online discussion forums ("Group(s)") in selected cities.

If you join a Group, then your membership in the Group as well as some of your public information (such as your profile picture and public profile page) will be visible to users who browse the Group. If you publish postings in a Group, then your postings will be visible to such users as well. The ability to browse the Group will depend on the Group settings, and it may or may not be limited to members of that Group.

Releasing Information

We may provide your personal information to other persons but only if:

we have your consent;
we provide the information to Service Providers who assist us in serving you and who have agreed to appropriate contractual provisions regarding the protection of personal information in accordance with applicable law; or
we are required or otherwise permitted to do so by law, regulation or court order.
We may send your personal information outside of the country for the purposes set out herein, including for process and storage by Service Providers in connection with such purposes, and you should note that while such information is out of the country, it is subject to the laws of the country in which it is held, and may be subject to disclosure to the governments, courts or law enforcement or regulatory agencies of such other country, pursuant to the laws of such country.

Business Transfers by Fava

If Fava undertakes or is involved in any merger, acquisition, reorganization, sale of assets or bankruptcy or insolvency event, then we may sell, transfer or share some or all of our assets, including your Personal Information. In this event, we will notify you before your Personal Information is transferred and becomes subject to a different privacy policy.

How to change or delete your information, or cancel your Fava account?

You may review, update, correct or delete the Personal Information in your Fava account. If you would like to correct your information or cancel your Fava account entirely, you can do so by contacting us at hello@Fava.com. Please also note that any reviews, forum postings and similar materials posted by you may continue to be publicly available on the Platform in association with your first name, even after your Fava account is cancelled.

Securing your personal information

We are continuously implementing and updating administrative, technical, and physical security measures to help protect your Personal Information against unauthorized access, destruction or alteration. However, no method of transmission over the Internet, and no method of storing electronic information, can be 100% secure. So, we cannot guarantee the absolute security of your transmissions to us and of your Personal Information that we store.

Respecting and Responding to Your Privacy Concerns

You have the option to refuse or withdraw consent to the collection, use and disclosure of your personal information, and we will respect your choices. If you wish to exercise this option or if you have any questions or enquiries with respect to our privacy policies or procedures, please send a written request to: Fava-support@googlegroups.com. We will investigate and respond to your concerns about any aspect of our handling of your information. Your opinion matters to us! If you'd like to provide feedback to us about this Privacy Policy, please email us at Fava-support@googlegroups.com

BY USING OUR SERVICES, YOU ACKNOWLEDGE THAT YOU HAVE READ OUR PRIVACY POLICY, UNDERSTAND IT AND AGREE TO ALL OF THE TERMS AND CONDITIONS IN OUR PRIVACY POLICY.